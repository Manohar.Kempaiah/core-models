<?php

namespace ddc\Date;

final class Date
{
    public function displayCurrentDateTime()
    {
        // Get the current date and time using the DateTime class
        $currentDateTime = new \DateTime();
        
        // Format the date and time as a string
        $formattedDateTime = $currentDateTime->format('Y-m-d H:i:s');
        
        echo 'Current Date and Time: ' . $formattedDateTime;
    }
}
